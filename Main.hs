{-# LANGUAGE TemplateHaskell #-}
module Main where

import           Control.Applicative ((<$>))
import           Control.Concurrent (threadDelay)
import           Control.Monad (forever, unless)
import           Data.Aeson (eitherDecodeStrict')
import qualified Data.ByteString as BS (readFile)
import           Data.Char (isSpace, isDigit, digitToInt)
import           Data.FileEmbed (embedFile)
import           Data.HashMap.Lazy (HashMap, (!))
import qualified Data.HashMap.Lazy as HM hiding ((!), HashMap)
import qualified Data.List as L
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T (readFile, writeFile, putStr)
import           Data.Version (showVersion)
import           Data.Yaml (decodeEither)
import           Hasmin (minifyCSS)
import           System.Directory (listDirectory, doesDirectoryExist, doesFileExist, makeAbsolute)
import           System.Exit (exitSuccess)
import qualified System.FSNotify as N
import           System.FilePath (takeExtension)

import           Conf
import           Options
import           Paths_cell (version)


{- Text Utilities -}


infixl 5 ~+~
(~+~) :: Text -> Text -> Text
(~+~) = T.append

safeT :: (Text -> Text) -> Text -> Text
safeT function text
	| T.null text	= ""
	| otherwise		= function text

safeHead :: Text -> Text
safeHead = safeT (T.singleton . T.head)

safeTail :: Text -> Text
safeTail = safeT T.tail

safeInit :: Text -> Text
safeInit = safeT T.init


{- Inititalizers -}


cellDefs :: CellDefMap
cellDefs = case eitherDecodeStrict' $(embedFile "css.json") of
	Left err -> error $ "Cell was compile with an invalid css.json, Error: " ++ err
	(Right cell_defs) -> cell_defs

readConfig :: FilePath -> IO Config
readConfig fp = do
	isConf <- doesFileExist fp
	confFile <- if isConf
		then BS.readFile fp
		else return "{}"
	case decodeEither confFile :: Either String Config of
		Left	err	-> error $ "Error decoding user config `" ++ fp ++ "`. Error: " ++ err
		Right conf -> return conf


{- Get markup files -}


-- Get all filepaths from a path.
getFilePaths :: FilePath -> IO [FilePath]
getFilePaths path = do
	dirExists <- doesDirectoryExist path
	if dirExists
		then do
			dir <- listDirectory path
			let fullpaths = ((path ++ "/") ++) <$> filter ((/= '.') . head) dir
			L.concat <$> mapM getFilePaths fullpaths
		else return [path]

-- Get all markup files in a path.
getMarkupFiles :: Config -> FilePath -> IO [Text]
getMarkupFiles conf path = do
	filepaths <- getFilePaths path
	let isMarkupFile = case _filetypes conf of
				(Just types) -> \filepath -> takeExtension filepath `elem` types
				_ -> \filepath -> takeExtension filepath `elem` [".html", ".htm"]
	case filter isMarkupFile filepaths of
		[] -> exitSuccess
		fs -> mapM T.readFile $ filter isMarkupFile fs


{- Prepare markup files -}


-- Prepare file for sieveing.
prepareFile :: Text -> [Text]
prepareFile = T.split sep
	where
		sep '\'' = True
		sep '"'	= True
		sep '<'	= True
		sep '>'	= True
		sep '='	= True
		sep '|'	= True
		sep chr = isSpace chr

-- Sieve for cell classes.
sieveCell :: CellDefMap -> [Text] -> [Text]
sieveCell defmap = filter isCell
	where
		isCell c =
			let (ident, body) = T.breakOn "(" c
			in HM.member ident defmap && T.isInfixOf ")" body


{- Make CSS class name from HTML class identifier -}


-- Escape illeagal CSS characters.
escapeCSS :: Text -> Text
escapeCSS = T.concatMap escape
	where
		escape '(' = "\\("
		escape ')' = "\\)"
		escape ',' = "\\,"
		escape '%' = "\\%"
		escape '#' = "\\#"
		escape '`' = "\\`"
		escape '!' = "\\!"
		escape ':' = "\\:"
		escape '/' = "\\/"
		escape '$' = "\\$"
		escape '.' = "\\."
		escape '@' = "\\@"
		escape chr = T.singleton chr

-- Expand pseudo class abbreviation if one exists.
expandPseudoClass :: Text -> Text
expandPseudoClass ident = T.append ident $ HM.lookupDefault "" abbr pseudoMap
	where
		abbr = T.takeWhile (/='-') $ T.dropWhile (/=':') ident

pseudoMap :: TextMap
pseudoMap = HM.fromList 
	[ (":a",":active")
	, (":c",":checked")
	, (":d",":default")
	, (":di",":disabled")
	, (":e",":empty")
	, (":en",":enabled")
	, (":fi",":first")
	, (":fc",":first-child")
	, (":fot",":first-of-type")
	, (":fs",":fullscreen")
	, (":f",":focus")
	, (":h",":hover")
	, (":ind",":indeterminate")
	, (":ir",":in-range")
	, (":inv",":invalid")
	, (":lc",":last-child")
	, (":lot",":last-of-type")
	, (":l",":left")
	, (":li",":link")
	, (":oc",":only-child")
	, (":oot",":only-of-type")
	, (":o",":optional")
	, (":oor",":out-of-range")
	, (":ro",":read-only")
	, (":rw",":read-write")
	, (":req",":required")
	, (":r",":right")
	, (":rt",":root")
	, (":s",":scope")
	, (":t",":target")
	, (":va",":valid")
	, (":vi",":visited") ]

-- Convert HTML class identifier to valid CSS class.
makeClassName :: Text -> Text
makeClassName ident = T.cons '.' $ expandPseudoClass $ escapeCSS ident

{- Process arguments -}

-- Split HTML class identifier into a list of Text arguments, retaining inner parentheses.
-- This function is the closest thing to pure evil that a haskell function can be.
argumentSplit :: Text -> [Text]
argumentSplit ident
	| T.isInfixOf "(" arg = parenSafeSplit arg
	| otherwise = T.splitOn "," arg
	where
		arg = safeTail.safeInit.T.dropWhile (/='(') $ T.dropWhileEnd (/=')') ident
		recurse p = fst p : argumentSplit (safeTail $ snd p)
		parenSafeSplit s
			| T.isInfixOf "(" (fst $ T.breakOn "," s) = recurse $ (\p -> (fst p ~+~ ")",safeTail $ snd p)) $ T.breakOn ")" s
			| otherwise = recurse $ T.breakOn "," s


{- Process templates -}


-- Get the value of the character that is the largest Int from a Text.
maxCharInt :: Text -> Int
maxCharInt text = fst $ T.mapAccumL count 0 text
	where
		count a c
			| isDigit c && (a < digitToInt c) = (digitToInt c,c)
			| otherwise = (a,c)

{- Substitute replacement-syntax with arguments.
e.g args=[solid,1px,#000] template="border: $0,$1,$2" -> "border: solid,1px,#000" -}
substitute :: [Text] -> Int -> Text -> Text
substitute [] _ t = t
substitute arguments count template
	| count > 0 = substitute arguments (count - 1) $ T.replace target argument template
	| otherwise = T.replace target argument template
	where
		target = " $" ~+~ T.pack (show count)
		argument
		 | count >= L.length arguments = ""
		 | otherwise = T.cons ' ' $ arguments !! count

-- Fill template by substituting cell arguments.
fillTemplate :: Text -> [Text] -> Text
fillTemplate template arguments = T.replace "`" "'" $ T.stripEnd $ substitute arguments (maxCharInt template) template


{- Prefixing -}


prefixProperty :: (Text,Text) -> Maybe (HashMap Text [Text]) -> Text
prefixProperty _ Nothing						= ""
prefixProperty (p,v) (Just cssrule) = case HM.lookup p cssrule of
	Nothing -> ""
	Just props -> T.concat $ fmap (\a -> a ~+~ v ~+~ "; ") props

prefixValue :: (Text,Text) -> Maybe (HashMap Text [Text]) -> Text
prefixValue _ Nothing						= ""
prefixValue (p,v) (Just cssrule) = case HM.lookup value cssrule of
	Nothing -> ""
	Just vals -> T.concat $ fmap (\a -> p ~+~ ": " ~+~ a ~+~ "; ") vals
	where
		value = T.dropWhile (\z -> z == ':' || z == ' ') v

applyPrefixes :: Config -> Text -> Text
applyPrefixes config preclass = T.concat $ fmap prefixes $ T.splitOn ";" $ safeInit preclass
	where
		prefixes c = prefixProperty parts (_prefixProperties config) ~+~ prefixValue parts (_prefixValues config) ~+~ c ~+~ ";"
			where
				parts = T.breakOn ":" c


{- Properties -}


-- Generate the CSS properties from the HTML identifier.
makeCSSProperties :: Config -> CellDef -> Text -> Text
makeCSSProperties conf (CellDef template ali) ident =
	applyPrefixes conf $ fillTemplate template $
		expandArgs (argumentSplit ident) $ makeExps ali $ _constants conf

makeExps :: Maybe TextMap -> Maybe TextMap -> TextMap
makeExps Nothing Nothing = HM.empty
makeExps Nothing (Just varsMap) = varsMap
makeExps (Just aliMap) Nothing = aliMap
makeExps (Just aliMap) (Just varsMap) = HM.union aliMap varsMap

expandArgs :: [Text] -> TextMap -> [Text]
expandArgs args exps = fmap expand args
	where
		expand x = HM.lookupDefault x x exps


{- Media Queries -}


-- Apply media query to generated CSS class.
applyMediaQuery :: Config -> Text -> Text -> Text
applyMediaQuery config ident cssclass = let bp = getBreakPoint ident in
	case makeMediaQuery bp $ _breakpoints config of
		Nothing	-> cssclass
		Just mq	-> mq ~+~ cssclass ~+~ "\n}"

-- Make media query Text.
makeMediaQuery :: Text -> Maybe TextMap -> Maybe Text
makeMediaQuery _ Nothing = Nothing
makeMediaQuery bp (Just bpm) = case HM.lookup bp bpm of
	Nothing -> Nothing
	Just val -> Just $ "@media (min-width: " ~+~ val ~+~ ") {\n"

-- Get media breakpoint if one exists, else empty Text.
getBreakPoint :: Text -> Text
getBreakPoint = safeTail.T.dropWhile (/='@').snd.T.breakOnEnd ")"


{- Classes -}


makeCSS :: CellDefMap -> Config -> Text -> Text
makeCSS defmap conf ident = applyMediaQuery conf ident $ makeClassName ident ~+~ classBody
	where
		abbr = fst $ T.breakOn "(" ident
		classBody = " {" ~+~ properties ~+~ "}"
			where
				properties
					| T.isInfixOf "!" ident = T.replace ";" " !important;" $ makeCSSProperties conf (defmap ! abbr) ident
					| otherwise = makeCSSProperties conf (defmap ! abbr) ident

processFile :: Config -> CellDefMap -> [Text] -> [Text]
processFile conf defmap file = makeCSS defmap conf <$> sieveCell defmap file

processFiles :: Config -> [Text] -> Text
processFiles conf files = T.unlines $ helpers ++ ["/* ---| Cells |--- */"] ++ standard
	where
		score l = "_" /= safeHead (safeTail l)
		prepared = fmap prepareFile files
		standard = L.sort $ L.nub $ concatMap (processFile conf cellDefs) prepared
		helpers = case _helpers conf of
			Nothing -> ["/* ---| No helpers defined in cell.yaml |--- */"]
			Just hlprs -> do
				let h = L.sortOn score $ L.sort $ L.nub $ concatMap (processFile conf hlprs) prepared
				if not (null h)
					then "/* ---| Helpers |--- */" : h
					else ["/* ---| No helpers used |--- */"]

process :: Config -> FilePath -> IO Text
process conf fp = processFiles conf <$> getMarkupFiles conf fp

main :: IO ()
main = do
	opts <- getOpts
	whenFlag Help opts $ do
		putStrLn
			"Cell CSS Generator, Help\n\n\
			\Examples: \n\
			\  cell <file|path> <options> > output.css\n\
			\  e.g `cell markupfolder/ > static/style.css`\n\
			\  or, `cell index.html > index.css`\n\
			\  or, `cell -w style.css templates/`\n\
			\  or, `cell --watch=style.css templates/`\n\
			\    to watch templates for changes.\n"
		putStrLn usage
		exitSuccess
	whenFlag Version opts $ do
		putStrLn $ "Cell CSS Generator, Version " ++ showVersion version
		exitSuccess
	conf <- readConfig "cell.yaml"
	let inFilePath = getArg 0 "." opts
	whenKey Watch opts $ \path -> do
		outFilePath <- makeAbsolute path
		manager <- N.startManager
		_ <- N.watchTree manager inFilePath (const True) $ \e -> 
			unless (outFilePath == N.eventPath e || L.isSuffixOf "~" (N.eventPath e)) $ do
				putStrLn $ "\nChange detected, regenerating `" ++ path ++ "`"
				process conf inFilePath >>= T.writeFile outFilePath
		putStrLn "Watching directory.."
		forever $ threadDelay 1000000
	css <- process conf inFilePath
	whenFlag Minify opts $ do
		let mini = minifyCSS css
		case mini of
			Right minicss -> T.putStr minicss >> exitSuccess
			Left er -> error er
	T.putStr css
