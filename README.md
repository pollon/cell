# Cell is a CSS generator.

Write CSS classes in your HTML, and cell generates *only* the required CSS.

It is intended to help you focus on design by generating CSS you need, instead 
of manually (mis)managing CSS you might need.


*How?*

**Here's a quick example**:

To get, say, a gray border around your header, use a [predefined](http://cellcss.com) keyword for
border "Bd" and give it explicit values in parenthesis "( )":
```
index.html
------------------------------------------------
<header class="Bd(s,1px,#CCC)">Give this header a solid, 1px, gray border</header>
```

In terminal run:
```
$ cell index.html > style.css
```

Cell generates `style.css` (with special characters properly escaped "\"):
```
style.css
-------------------------------------------------
.Bd\(s\,1px\,\#CCC\) { border: solid 1px #CCC }
```


# Installation

Just download the
[static binary](https://gitlab.com/thehands/cell/raw/666fb3980fbc287a3ddc069ac75dea22cc560cba/cell) anywhere
and copy to somewhere on your path. e.g `sudo cp cell /usr/bin/`

An example cell configuration with recommended prefixing is available [here](https://gitlab.com/thehands/cell/raw/master/cell.yaml)

Anyone interested in packing Cell for a Linux distro
is welcome to do so.

# FAQ
**Q:** *What is cell built with?*

**A:** Haskell

**Q:** *How am I supposed to remember all those keywords?*

**A:** You don't have to, you can reference them quickly at the [reference site](http://cellcss.com)
