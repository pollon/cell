module Conf where

import           Control.Applicative ((<$>))
import           Data.HashMap.Lazy (HashMap)
import           Data.Text (Text)
import           Data.Yaml

type CellDefMap = HashMap Text CellDef
type TextMap = HashMap Text Text

data CellDef = CellDef Text (Maybe TextMap)
	deriving (Eq, Show)

instance FromJSON CellDef where
  parseJSON (Object v) = CellDef <$>
    v .:  "template" <*>
    v .:? "aliases"
  parseJSON _ = error "Expected JSON of type Object."

{- Config record

The Config record holds data read from the
local config file, by default it looks for a `cell.yaml` or `cell.yml`
manually specify a file with `-c`.

-}

data Config = Config
	{ _breakpoints :: Maybe TextMap
	, _filetypes :: Maybe [String]
	, _prefixProperties :: Maybe (HashMap Text [Text])
	, _prefixValues :: Maybe (HashMap Text [Text])
	, _constants :: Maybe TextMap
	, _helpers :: Maybe CellDefMap
	} deriving (Eq, Show)

instance FromJSON Config where
	parseJSON (Object v) = Config <$>
		v .:? "Breakpoints" <*>
		v .:? "FileTypes" <*>
		v .:? "PrefixProperties" <*>
		v .:? "PrefixValues" <*>
		v .:? "Variables" <*>
		v .:? "Helpers"
	parseJSON _ = error "Error parsing config, expected YAML."
