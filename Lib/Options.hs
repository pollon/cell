module Options
	( module Options
	, module Data.Maybe
	) where

import qualified Data.List as L (find)
import           Data.Maybe (fromMaybe)
import           System.Console.GetOpt
import           System.Environment (getArgs)

data Symbol = Version | Help | Watch | Minify | Config
	deriving (Eq, Show)

data Opt = Flag Symbol | Key Symbol String
	deriving (Eq, Show)

options :: [OptDescr Opt]
options =
	[ Option ['v','V'] ["version"] (NoArg (Flag Version)) "Show version number"
	, Option ['h'] ["help"] (NoArg (Flag Help)) "Show help message"
	, Option ['m'] ["minify"] (NoArg (Flag Minify)) "Should minify output."
	, Option ['c'] ["config"] (ReqArg	(Key Config) "<config>") "Specify config file tp use."
	, Option ['w'] ["watch"] (ReqArg (Key Watch) "<output>") "Watch directory for changes and run cell each for each."
	]

usage :: String
usage = usageInfo "Options:" options

getOpts :: IO ([Opt], [String])
getOpts = do
	args <- getArgs
	case getOpt Permute options args of
		(o,n,[])	 -> return (o,n)
		(_,_,errs) -> ioError (userError (concat errs ++ usage))

matchSymbol :: Symbol -> Opt -> Bool
matchSymbol sym (Flag s	) = sym == s
matchSymbol sym (Key	s _) = sym == s

whenFlag :: (Applicative f) => Symbol -> ([Opt], [String]) -> f () -> f ()
whenFlag sym (ol,_) a = case L.find (matchSymbol sym) ol of
	Just (Flag _) -> a
	_ -> pure ()

whenKey :: (Applicative f) => Symbol -> ([Opt], [String]) -> (String -> f ()) -> f ()
whenKey sym (ol,_) a = case L.find (matchSymbol sym) ol of
	Just (Key _ v) -> a v
	_ -> pure ()

getArg :: Int -> String -> ([Opt], [String]) -> String
getArg index def (_,args) = if length args < index + 1
	then def
	else args !! index
